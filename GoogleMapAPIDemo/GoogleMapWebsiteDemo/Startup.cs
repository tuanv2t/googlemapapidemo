﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GoogleMapWebsiteDemo.Startup))]
namespace GoogleMapWebsiteDemo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
